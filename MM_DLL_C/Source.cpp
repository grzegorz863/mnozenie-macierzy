void mm(float *macierz1, float *macierz2, float *macierz_wynikowa, int lkm1, int lwm1, int lkm2, int lwm2, int kw)
{
	int i = 0, j = 0, k = 0, mw_index = 0;
	float s = 0;

	for (i = 0; i < lkm1*lwm1; i += lkm1)
		for (j = 0; j < lkm2*lwm2; j += lwm2)
		{
			s = 0;
			for (k = 0; k < lkm1; k++)
				s += macierz1[i + k] * macierz2[k + j];
			macierz_wynikowa[kw] = s;
			kw++;
		}
}

extern "C" __declspec(dllexport) void mnozenie_macierzy(float *macierz1, float *macierz2, float *macierz_wynikowa, int lkm1, int lwm1, int lkm2, int lwm2, int kw)
{
	mm(macierz1, macierz2, macierz_wynikowa, lkm1, lwm1, lkm2, lwm2, kw);
}