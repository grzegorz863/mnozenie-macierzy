.CODE
mm proc a: DWORD, b: DWORD, c:DWORD, d:DWORD, e:DWORD, f:DWORD, g:DWORD, h:DWORD
	
	;-----------------------------------------------
	; zapisanie potrzebnych rejestrow na stosie
		push rbp		;przechowanie rbp na stosie
		push rdi		;przechowanie rdi na stosie
		push r15		;przechowanie r15 na stosie

	;-----------------------------------------------
	; Zapisanie wska�nik�w do macierzy
		mov r12, rcx		;wska�nik do macierzy 1
		mov r13, rdx		;wska�nik do macierzy 2
	
	;-----------------------------------------------
	;pobieranie argument�w z rejestr�w i wpisywanie do RAX
		movd mm1, r9	;liczba kolumn macierzy 1 - lkm1
		movd mm2, e		;liczba wierszy macierzy 1 - lwm1
		mov ebx, f		;liczba kolumn macierzy 2 - lkm2
		mov eax, g		;liczba wierszy macierzy 2 - lwm2
		mov r14, rax	;liczba wierszy macierzy 2 - lwm2

	;-----------------------------------------------
	; wyznaczenie ogranicznika petli 1, trzymany w mm0
		
		movq mm7, mm1		;skopiowanie lkm1 do mm7 aby nie nadpisa� mm1
		PMULUDQ mm7, mm2	;pomno�enie lkm1 i lwm1
		movq mm0, mm7		;zapsanie warto�ci mno�enia w mm0, jest to ogranicznik p�tli 1
	
	;-----------------------------------------------
	; wyznaczenie odpowiedniego adresu zapisu wynikow
		mov eax, h		;h - pocz�tkowa pozycja zapisu wyniku w danym w�tku, potrzebne do wielow�tkowosci
		shl eax, 2		;przemno�enie przez 4
		add r8, rax		;zapisanie iloczynu do rejestru r8

	;-----------------------------------------------
	; Algorytm mnozenia macierzy
		
	mov r9, 0								;licznik petli 1 - lp1
	loop1:
		mov r10, 0							;licznik petli 2 - lp2
		mov rax, rbx						;skopiowanie lkm2 do rax aby nie nadpisa� rbx - wyznaczanie ogranicznika p�tli 2 (lkm2*lwm2)
		mul r14								;wykonanie mno�enia (lkm2*lwm2)
		mov rcx, rax						;zapisanie wyniku do rcx - ogranicznik p�tli 2
		loop2:
			mov r11, 0						;licznik petli 3 - lp3
			xorps xmm3, xmm3				;wyzerowanie rejestru xmm3, by m�c do niego dodawa�
			movd r15, mm1					;wyznaczenie ogranicznika petli 3 - lkm1
			loop3:
				;wyliczanie pozycji czytanie z pami�ci (macierz1[lp1 + lp3])
				mov rax, r9					;skopiowanie lp1 do rax aby wykona� dodanie lp3
				add rax, r11				;dodanie lp3
				vmovups ymm0, ymmword ptr [r12+rax*4]		;odczyt z pami�ci 8 liczb macierzy 1 do rejestru ymm0
				
				;wyliczanie pozycji czytanie z pami�ci (macierz2[lp2 + lp3])
				mov rax, r10								;skopiowanie lp2 do rax aby wykona� dodanie lp3
				add rax, r11								;dodanie lp3
				vmovups ymm1, ymmword ptr [r13 + rax*4]		;odczyt z pami�ci 8 liczb macierzy 2 do rejestru ymm1
				
				;Operacje wektorowe - mno�enie
				vmulps ymm0, ymm1, ymm0						;pomno�enie wektor�w ymm0 i ymm1 tak aby wynik by� zapisany do ymm0 - naraz mno�one jest 8 elemen�w macierzy 1 i 2
				vxorps ymm2, ymm2, ymm2						;wyzerowanie ymm2 aby m�c p�niej wykorzyta� ten rejestr 
				vxorps ymm1, ymm1, ymm1						;wyzerowanie ymm1 aby m�c p�niej wykorzyta� ten rejestr 
				
					;Operacje wektorowe - dodawanie element�w wektora
				vhaddps ymm0, ymm0, ymm2					;sumowanie element�w rejestru ymm0 po przez dodawanie na przemienne wyzerowanego ymm2
					;po wykonaniu rozkazu (FX -> YMM0): F0 = 0; F1 = 0; F2 = F0 + F1; F3 = F2 + F3; F4 = 0; F5 = 0; F6 = F4 + F5; F7 = F6 + F7
				
				vhaddps ymm0, ymm0, ymm2					;sumowanie element�w rejestru ymm0 po przez dodawanie na przemienne wyzerowanego ymm2
					;po wykonaniu rozkazu (FX = YMM0FX): F0 = 0; F1 = 0; F2 = 0; F3 = F3 + F2 + F1 + F0; F4 = 0; F5 = 0; F6 = 0; F7 = F7 + F6 + F5 + F4
				
				vextractf128 xmm1, ymm0, 11111111b			;przepisanie g�rnej po��wki rejestru ymm0 do xmm1
					;XMM10 = F7 + F6 + F5 + F4; XMM00 =  F3 + F2 + F1 + F0
				vaddss xmm0, xmm1,xmm0			;dodanie F7 + F6 + F5 + F4 i  F3 + F2 + F1 + F0 w XMM00 jest suma ymm0
				vaddss xmm3, xmm3, xmm0			;sumowanie mno�enia i dodawania wektor�w na koniec p�tli
				
				add r11, 8					;inkrementacje lp3 o 8, poniewa� pomno�one zosta�o 8 liczb 
				sub r15, 8					;dekrementacje ogranicznika p�tli 3 o 8, poniewa� pomno�one zosta�o 8 liczb 
			jnz loop3						;je�eli ogranicznik p�tli 3 wynosi 0 to koniec p�tli

			movss dword ptr[r8], xmm3		;zapis wyniku mnozenia wiersza macierzy 1 z kolumn� macierzy 2
			add r8, 4						;dodanie 4 do wska�nika na macierz wynikow�, przygotowanie do nast�pnego zapisu wyniku (float to 4 bajty)
			add r10, r14					;inkremantacja lp2
			sub rcx, r14					;dekrementacja ogranicznika p�tli 2 o lwm2
		jnz loop2							;koniec p�tli 2 je�li ZR = 1
				
		movd mm7, r9					;skoiowanie lp1 do mm7 aby m�c go zinkrementowa� o lkm1
		PADDq mm7, mm1					;inkrementacja lp1 o lkm1
		movd r9, mm7					;aktualizacja lp1 now� warto�ci�
		PSUBq mm0, mm1					;dekrementacja ogranicznika p�tli 1 o lkm1
		movd rax, mm0					;skopiowanie ogranicznika p�tli 1 �eby m�c sprawdzi� czy r�wny jest 0
		sub rax, 0						;skrawdzenie czy ogranicznik macierzy 1 r�wny jest 0, je�eli tak to zostanie podniesiona flaga ZR
	jnz loop1							;koniec p�tli 1 je�li ZR = 1

	;-----------------------------------------------
	;pobranie rejestrow ze stosu
		pop r15			;nadpisanie rejestru r15 pocz�tkow� warto�ci�
		pop rdi			;nadpisanie rejestru rdi pocz�tkow� warto�ci�
		pop rbp			;nadpisanie rejestru rbp pocz�tkow� warto�ci�

	ret			;powr�t z procedury mno�enia macierzy
mm endp			;KONIEC
END				;KONIEC