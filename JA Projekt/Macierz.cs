﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JA_Projekt
{
	class Macierz
	{
		float[] tab_macierz;
		int liczba_kolumn;
		int liczba_wierszy;

		public Macierz(float[] _tab_macierz, int _liczba_kolumn, int _liczba_wierszy)
		{
			this.tab_macierz = _tab_macierz;
			this.liczba_kolumn = _liczba_kolumn;
			this.liczba_wierszy = _liczba_wierszy;
		}

		public float[] MacierzTablica
		{
			get
			{
				return tab_macierz;
			}
			set
			{
				tab_macierz = value;
			}
		}

		public int LiczbaKolumn
		{
			get
			{
				return liczba_kolumn;
			}
			set
			{
				liczba_kolumn = value;
			}
		}

		public int LiczbaWierszy
		{
			get
			{
				return liczba_wierszy;
			}
			set
			{
				liczba_wierszy = value;
			}
		}

		public override string ToString()
		{
			string m = "";
			foreach (int element in tab_macierz)
			{
				m += element.ToString()+" ";
			}
			m.Remove(m.LastIndexOf(' '), 1);
			return m;
		}
	}
}
