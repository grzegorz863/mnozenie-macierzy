﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JA_Projekt
{
	class MnozenieMacierzyNieJestMozliwe : Exception
	{
		string wiadomosc;

		public MnozenieMacierzyNieJestMozliwe(string msg)
		{
			this.wiadomosc = msg;
		}

		public override string Message
		{
			get
			{
				return this.wiadomosc;
			}
		}
	}
}
