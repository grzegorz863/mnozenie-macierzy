﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace JA_Projekt
{
	sealed class Program_w_Assemblerze : ProgramDLL
	{
		[DllImport("MM_DLL_Assembler.dll", EntryPoint = "mm")]
		public static extern void mnozenie_macierzy(float[] macierz1, float[] macierz2, float[] macierz_wynikowa, int liczba_kolumn_macierzy1, int liczba_wierszy_macierzy1, int liczba_kolumn_macierzy2, int liczba_wierszy_macierzy2, int ktory_wynik);

		//-----------------------------------------------------------------

		const int ROZMIAR_AVX = 8;

		public Program_w_Assemblerze(Macierz _macierz1, Macierz _macierz2, int _liczba_watkow)
		{
			this.macierz1 = _macierz1;
			this.macierz2 = _macierz2;
			this.LiczbaWatkow = _liczba_watkow;
		}

		private void DostosujMacierzeDoAVX()
		{
			int liczba_pol_do_dodania = this.macierz2.LiczbaWierszy % ROZMIAR_AVX;
			liczba_pol_do_dodania = ROZMIAR_AVX - liczba_pol_do_dodania;
			int ilosc_pelnych_osemek_w_wierszu = this.macierz2.LiczbaWierszy / ROZMIAR_AVX;
			int rozmiar_tablicy = ((ilosc_pelnych_osemek_w_wierszu + 1) * ROZMIAR_AVX) * this.macierz2.LiczbaKolumn;

			float[] nowa_tablica = new float[rozmiar_tablicy];
			for (int i = 0; i < this.macierz2.LiczbaKolumn; i++)
			{
				for (int j = i * macierz2.LiczbaWierszy; j < i * macierz2.LiczbaWierszy + this.macierz2.LiczbaWierszy; j++) 
				{
					int dodajnik = i * liczba_pol_do_dodania;
					nowa_tablica[j+dodajnik] = this.macierz2.MacierzTablica[j];
				}
			}

			this.macierz2.MacierzTablica = nowa_tablica;
			this.macierz2.LiczbaWierszy = (ilosc_pelnych_osemek_w_wierszu + 1) * ROZMIAR_AVX;


			liczba_pol_do_dodania = this.macierz1.LiczbaKolumn % ROZMIAR_AVX;
			liczba_pol_do_dodania = ROZMIAR_AVX - liczba_pol_do_dodania;
			int ilosc_pelnych_osemek_w_kolumnie = this.macierz1.LiczbaKolumn / ROZMIAR_AVX;
			rozmiar_tablicy = ((ilosc_pelnych_osemek_w_kolumnie + 1) * ROZMIAR_AVX) * this.macierz1.LiczbaWierszy;
			nowa_tablica = new float[rozmiar_tablicy];

			for (int i = 0; i < this.macierz1.LiczbaWierszy; i++)
			{
				for (int j = i * macierz1.LiczbaKolumn; j < i * macierz1.LiczbaKolumn + this.macierz1.LiczbaKolumn; j++)
				{
					int dodajnik = i * liczba_pol_do_dodania;
					nowa_tablica[j + dodajnik] = this.macierz1.MacierzTablica[j];
				}
			}
			
			this.macierz1.MacierzTablica = nowa_tablica;
			this.macierz1.LiczbaKolumn = (ilosc_pelnych_osemek_w_kolumnie + 1) * ROZMIAR_AVX;
		}

		public  Macierz Wykonaj()
		{
			/*
			string s = "C:\\Users\\Grzegorz\\Desktop\\macierzgen.txt";	//do usuniecia
			OdczytZPliku odczyt = new OdczytZPliku(s);	//do usuniecia
			Macierz[] dwie_macierze = odczyt.OdczytajMacierze();	//do usuniecia
			this.macierz1 = dwie_macierze[0];	//do usuniecia
			this.macierz2 = dwie_macierze[1];	//do usuniecia
			*/
			float[] macierz_wynikowa = new float[this.macierz1.LiczbaWierszy * this.macierz2.LiczbaKolumn];
			List<Task> watki = new List<Task>(this.LiczbaWatkow);
			
			if((this.macierz2.LiczbaWierszy%8)!=0)
				this.DostosujMacierzeDoAVX();

			Stopwatch sw = new Stopwatch();
			
			if (this.LiczbaWatkow >= this.macierz1.LiczbaWierszy)
			{
				sw.Start();
				for (int i = 0; i < this.macierz1.LiczbaWierszy; i++)
				{
					float[] wiersz = this.WierszMacierzy1(macierz1.LiczbaKolumn, macierz1.MacierzTablica, i, macierz1.LiczbaKolumn);
					int numer_wyniku = i * macierz2.LiczbaKolumn;
					watki.Add(Task.Run(() => mnozenie_macierzy(wiersz, macierz2.MacierzTablica, macierz_wynikowa, macierz1.LiczbaKolumn, 1, macierz2.LiczbaKolumn, macierz2.LiczbaWierszy, numer_wyniku)));
				}
			}
			else
			{
				int domyslna_liczba_linii_na_watek = macierz1.LiczbaWierszy / this.LiczbaWatkow;
				int pozostała_liczba_linii = macierz1.LiczbaWierszy % this.LiczbaWatkow;
				int[] liczba_linii_na_watek = Enumerable.Repeat(domyslna_liczba_linii_na_watek, this.LiczbaWatkow).ToArray();

				int j = 0;
				for (int i = pozostała_liczba_linii; i > 0; i--, j++)
					liczba_linii_na_watek[j]++;

				int numer_wiersza2 = 0, numer_wyniku2 = 0;
				sw.Start();
				for (int i = 0; i < this.LiczbaWatkow; i++)
				{
					float[] wiersz = this.WierszMacierzy1(macierz1.LiczbaKolumn * liczba_linii_na_watek[i], macierz1.MacierzTablica, numer_wiersza2, macierz1.LiczbaKolumn);
					numer_wiersza2 += liczba_linii_na_watek[i];
					int a = i;
					int numer_wyniku = numer_wyniku2;
					watki.Add(Task.Run(() =>
					mnozenie_macierzy(wiersz, macierz2.MacierzTablica, macierz_wynikowa, macierz1.LiczbaKolumn, liczba_linii_na_watek[a], macierz2.LiczbaKolumn, macierz2.LiczbaWierszy, numer_wyniku)));
					numer_wyniku2 += macierz2.LiczbaKolumn * liczba_linii_na_watek[i];
				}

			}

			Task.WaitAll(watki.ToArray());
			sw.Stop();

			double ilosc_tikow = sw.ElapsedMilliseconds;
			this.czas_wykonania = (double)ilosc_tikow;
			return new Macierz(macierz_wynikowa, this.macierz1.LiczbaWierszy, this.macierz2.LiczbaKolumn);

		}

	}
}
