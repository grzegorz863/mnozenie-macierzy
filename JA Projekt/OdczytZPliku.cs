﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace JA_Projekt
{
	class OdczytZPliku
	{
		string sciezka_do_pliku;

		public OdczytZPliku(string sciezka)
		{
			this.sciezka_do_pliku = sciezka;
			this.KontrolujIstnieniePlikuIRozszerzenie();
		}

		private void KontrolujIstnieniePlikuIRozszerzenie()
		{
			if (!File.Exists(this.sciezka_do_pliku))
				throw new FileNotFoundException("Nie znaleziono podanego pliku!");

			if (!Regex.IsMatch(this.sciezka_do_pliku, @"\.txt$"))
				throw new FileFormatException("Nie jest to plik .txt!");
		}

		public Macierz[] OdczytajMacierze()
		{
			const int NR_PIERWSZEJ_MACIERZY = 0;
			const int NR_DRUGIEJ_MACIERZY = 1;

			FileStream fs = new FileStream(this.sciezka_do_pliku, FileMode.Open, FileAccess.Read);
			StreamReader sr = new StreamReader(fs);

			string caly_plik = sr.ReadToEnd();
			sr.Close();
			string[] m = caly_plik.Split('='); char[] nowa_linia = { '\r', '\n' };
			string[] m1 = m[NR_PIERWSZEJ_MACIERZY].Split(nowa_linia, StringSplitOptions.RemoveEmptyEntries);
			string[] m2 = m[NR_DRUGIEJ_MACIERZY].Split(nowa_linia, StringSplitOptions.RemoveEmptyEntries);

			int liczba_kolumn_macierzy1 = m1[0].Count(Char.IsWhiteSpace) + 1;
			int liczba_kolumn_macierzy2 = m2[0].Count(Char.IsWhiteSpace) + 1;
			int liczba_wierszy_macierzy1 = m1.Length;
			int liczba_wierszy_macierzy2 = m2.Length;

			if (liczba_kolumn_macierzy1 != liczba_wierszy_macierzy2)
				throw new MnozenieMacierzyNieJestMozliwe("Wykanonie mnożenia macierzy nie jest możliwe!");

			float[] macierz1 = new float[liczba_kolumn_macierzy1 * liczba_wierszy_macierzy1];
			float[] macierz2 = new float[liczba_kolumn_macierzy2 * liczba_wierszy_macierzy2];
			int i = 0;

			foreach (string element in m1) //wypelnianie macierzy 1
			{
				string[] linia = element.Split(' ');
				foreach(string liczba in linia)
				{
					macierz1[i] = float.Parse(liczba, System.Globalization.CultureInfo.InvariantCulture);
					i++;
				}
			}

			int j = 0;
			i = 0;
			float[,] m2_pom = new float[liczba_wierszy_macierzy2 , liczba_kolumn_macierzy2];
			foreach (string element in m2)
			{
				string[] linia = element.Split(' ');
				foreach(string liczba in linia)
				{
					m2_pom[i,j] = float.Parse(liczba, System.Globalization.CultureInfo.InvariantCulture);
					j++;
				}
				i++;
				j = 0;
			}
			int t = 0;
			for(i = 0;i<liczba_kolumn_macierzy2;i++)
			{
				for(j=0;j<liczba_wierszy_macierzy2;j++)
				{
					macierz2[t] = m2_pom[j, i];
					t++;
				}
			}
			
			Macierz macierz1_zwrot = new Macierz(macierz1, liczba_kolumn_macierzy1, liczba_wierszy_macierzy1);
			Macierz macierz2_zwrot = new Macierz(macierz2, liczba_kolumn_macierzy2, liczba_wierszy_macierzy2);
			Macierz[] macierze = { macierz1_zwrot, macierz2_zwrot };
			return macierze;
		}
	}
}
