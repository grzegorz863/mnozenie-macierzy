﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace JA_Projekt
{
	class ZapisDoPliku
	{
		private string sciezka;

		public ZapisDoPliku(string _sciezka)
		{
			this.sciezka = _sciezka;
		}

		public void ZapiszMacierz(Macierz macierz_wynikowa)
		{
			if (!Regex.IsMatch(this.sciezka, @"\.txt$"))
				throw new FileFormatException("Nie jest to plik .txt!");

			if (!File.Exists(this.sciezka))
			{
				File.Create(this.sciezka);
			}

			FileStream fs = new FileStream(this.sciezka, FileMode.Truncate, FileAccess.Write);
			StreamWriter sw = new StreamWriter(fs);
			string wiersz = "";
			for (int j = 0; j < macierz_wynikowa.LiczbaKolumn * macierz_wynikowa.LiczbaWierszy; j += macierz_wynikowa.LiczbaKolumn)
			{
				for (int i = 0; i < macierz_wynikowa.LiczbaKolumn; i++)
				{
					wiersz += macierz_wynikowa.MacierzTablica[i + j].ToString() + " ";
				}
				sw.WriteLine(wiersz.Remove(wiersz.LastIndexOf(' '), 1));
				wiersz = "";
			}
			sw.Flush();
			sw.Close();

		}
	}
}
