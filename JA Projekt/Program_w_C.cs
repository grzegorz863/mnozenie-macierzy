﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace JA_Projekt
{
	sealed class Program_w_C : ProgramDLL
	{
		[DllImport("MM_DLL_C.dll")]
		public static extern void mnozenie_macierzy(float[] macierz1, float[] macierz2, float[] macierz_wynikowa, int liczba_kolumn_macierzy1, int liczba_wierszy_macierzy1, int liczba_kolumn_macierzy2, int liczba_wierszy_macierzy2, int ktory_wynik);
		//-----------------------------------------------------------------


		public Program_w_C(Macierz _macierz1, Macierz _macierz2, int _liczba_watkow)
		{
			this.macierz1 = _macierz1;
			this.macierz2 = _macierz2;
			this.LiczbaWatkow = _liczba_watkow;
		}

		public Macierz Wykonaj()
		{
			/*			
			string s = "C:\\Users\\Grzegorz\\Desktop\\macierzgen.txt";   //do usuniecia
			OdczytZPliku odczyt = new OdczytZPliku(s);  //do usuniecia
			Macierz[] dwie_macierze = odczyt.OdczytajMacierze();    //do usuniecia
			this.macierz1 = dwie_macierze[0];   //do usuniecia
			this.macierz2 = dwie_macierze[1];   //do usuniecia
			*/
			float[] macierz_wynikowa = new float[this.macierz1.LiczbaWierszy * this.macierz2.LiczbaKolumn];
			List<Task> watki = new List<Task>(this.LiczbaWatkow);
			Stopwatch sw = new Stopwatch();

			if (this.LiczbaWatkow >= this.macierz1.LiczbaWierszy)
			{
				sw.Start();
				for (int i = 0; i < this.macierz1.LiczbaWierszy; i++)
				{
					float[] wiersz = this.WierszMacierzy1(macierz1.LiczbaKolumn, macierz1.MacierzTablica, i, macierz1.LiczbaKolumn);
					int numer_wyniku = i * macierz2.LiczbaKolumn;
					watki.Add(Task.Run(() => mnozenie_macierzy(wiersz, macierz2.MacierzTablica, macierz_wynikowa, macierz1.LiczbaKolumn, 1, macierz2.LiczbaKolumn, macierz2.LiczbaWierszy, numer_wyniku)));
				}
			}
			else
			{
				int domyslna_liczba_linii_na_watek = macierz1.LiczbaWierszy / this.LiczbaWatkow;
				int pozostała_liczba_linii = macierz1.LiczbaWierszy % this.LiczbaWatkow;
				int[] liczba_linii_na_watek = Enumerable.Repeat(domyslna_liczba_linii_na_watek, this.LiczbaWatkow).ToArray();

				int j = 0;
				for (int i = pozostała_liczba_linii; i > 0; i--, j++)
					liczba_linii_na_watek[j]++;

				int numer_wiersza2 = 0, numer_wyniku2 = 0;
				sw.Start();
				for (int i = 0; i < this.LiczbaWatkow; i++)
				{
					float[] wiersz = this.WierszMacierzy1(macierz1.LiczbaKolumn * liczba_linii_na_watek[i], macierz1.MacierzTablica, numer_wiersza2, macierz1.LiczbaKolumn);
					numer_wiersza2 += liczba_linii_na_watek[i];
					int a = i;
					int numer_wyniku = numer_wyniku2;
					watki.Add(Task.Run(() =>
					mnozenie_macierzy(wiersz, macierz2.MacierzTablica, macierz_wynikowa, macierz1.LiczbaKolumn, liczba_linii_na_watek[a], macierz2.LiczbaKolumn, macierz2.LiczbaWierszy, numer_wyniku)));
					numer_wyniku2 += macierz2.LiczbaKolumn * liczba_linii_na_watek[i];
				}

			}

			Task.WaitAll(watki.ToArray());
			sw.Stop();
			double ilosc_tikow = sw.ElapsedMilliseconds;
			this.czas_wykonania = (double)ilosc_tikow;
			return new Macierz(macierz_wynikowa, this.macierz1.LiczbaWierszy, this.macierz2.LiczbaKolumn);
		}
	}
}