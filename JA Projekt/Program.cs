﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace JA_Projekt
{
	class ProgramDLL
	{
		protected double czas_wykonania;
		protected int liczba_watkow;

	 	protected Macierz macierz1;
		protected Macierz macierz2;
		
		public double CzasWykonania
		{
			get
			{
				return this.czas_wykonania;
			}
		}

		public int LiczbaWatkow
		{
			get
			{
				return this.liczba_watkow;
			}

			set
			{
				this.liczba_watkow = value;
			}
		}

		protected float[] KolumnaMacierzy2(int liczba_wierszy, float[] macierz2, int numer_kolumny)
		{
			float[] kolumna = new float[liczba_wierszy];
			int j = 0;
			for (int i = numer_kolumny * liczba_wierszy; i < liczba_wierszy + numer_kolumny * liczba_wierszy; i++, j++)
			{
				kolumna[j] = macierz2[i];
			}
			return kolumna;
		}

		protected float[] WierszMacierzy1(int liczba_pozycji_do_pobrania, float[] macierz1, int numer_wiersza, int liczba_kolumn)
		{
			float[] wiersz = new float[liczba_pozycji_do_pobrania];
			int j = 0;
			for (int i = numer_wiersza * liczba_kolumn; i < liczba_pozycji_do_pobrania + numer_wiersza * liczba_kolumn; i++, j++)
			{
				wiersz[j] = macierz1[i];
			}
			return wiersz;
		}

	}
}

