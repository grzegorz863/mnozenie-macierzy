﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Management;

namespace JA_Projekt
{
	public partial class MainWindow : Window
	{
		private int LiczbaWatkow = 1;
		private Macierz macierz1;
		private Macierz macierz2;
		private Macierz macierz_wynikowa;
		private string sciezka_pliku_do_zapisu;
		private bool anulowano = true;

		public MainWindow()
		{
			InitializeComponent();
			RB_assembler.Checked += RB_Checked;
			RB_c.Checked += RB_Checked;
			this.Closed += MainWindow_Closed;
			TB_nazwa_procesora.Text = NazwaProcesora() + Environment.NewLine+ "Liczba rdzenii: " + Environment.ProcessorCount; // <-- pobranie liczby rdzenii
		}

		private void B_wyb_zrodlo_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog OknoOFD = new OpenFileDialog();
			OknoOFD.Multiselect = false;
			OknoOFD.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
			OknoOFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			OknoOFD.ShowDialog();
			try
			{
				
				OdczytZPliku odczyt = new OdczytZPliku(OknoOFD.FileName);
				TB_zrodlo.Text = OknoOFD.FileName;
				Macierz[] dwie_macierze = odczyt.OdczytajMacierze();
				this.macierz1 = dwie_macierze[0];
				this.macierz2 = dwie_macierze[1];
			}
			catch (MnozenieMacierzyNieJestMozliwe error)
			{
				MessageBox.Show(error.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			catch (FileFormatException error)
			{
				MessageBox.Show(error.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			catch (IndexOutOfRangeException error)
			{
				MessageBox.Show(error.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			catch (FileNotFoundException) { }
		}

		private string NazwaProcesora()
		{
			var cpu = new ManagementObjectSearcher("select * from Win32_Processor")
				.Get()
				.Cast<ManagementObject>()
				.First();
			return (string)cpu["Name"];

		}

		private void B_wyb_docel_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog OknoSFD = new SaveFileDialog();
			OknoSFD.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
			OknoSFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			OknoSFD.ShowDialog();
			this.sciezka_pliku_do_zapisu = OknoSFD.FileName;
			TB_docel.Text = OknoSFD.FileName;
		}

		private void RB_Checked(object sender, RoutedEventArgs e)
		{
			B_wykonaj.IsEnabled = true;
		}

		private void B_pomoc_Click(object sender, RoutedEventArgs e)
		{
			Pomoc pom = new Pomoc();
			pom.Show();
			this.IsEnabled = false;
			pom.Closed += Pom_Closed;
		}

		private void Pom_Closed(object sender, EventArgs e)
		{
			this.IsEnabled = true;
		}

		private void B_anuluj_Click(object sender, RoutedEventArgs e)
		{
			this.anulowano = true;
			this.Close();
		}

		private void B_zapis_Click(object sender, RoutedEventArgs e)
		{
			this.anulowano = false;
			this.Close();
		}

		private void B_wykonaj_Click(object sender, RoutedEventArgs e)
		{
			B_zapis.IsEnabled = true;
			try
			{
				if (RB_assembler.IsChecked.Value)
				{
					Program_w_Assemblerze program = new Program_w_Assemblerze(this.macierz1, this.macierz2, LiczbaWatkow);
					macierz_wynikowa = program.Wykonaj();
					TB_assem_czas.Text = program.CzasWykonania.ToString() + " ms";
				}
				if (RB_c.IsChecked.Value)
				{
					Program_w_C program = new Program_w_C(this.macierz1, this.macierz2, LiczbaWatkow);
					macierz_wynikowa = program.Wykonaj();
					TB_c_czas.Text = program.CzasWykonania.ToString() + " ms";


				}
			}
			catch(NullReferenceException)
			{
				MessageBox.Show("Najpierw wybierz plik z macierzami do pomnożenia!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private void B_wat_up_Clicked(object sender, RoutedEventArgs e)
		{
			if (LiczbaWatkow >= 1 && LiczbaWatkow < 32)
			{
				LiczbaWatkow++;
				TB_licz_wat.Text = LiczbaWatkow.ToString();
			}
		}

		private void B_wat_down_Clicked(object sender, RoutedEventArgs e)
		{
			if (LiczbaWatkow > 1 && LiczbaWatkow <= 32)
			{
				LiczbaWatkow--;
				TB_licz_wat.Text = LiczbaWatkow.ToString();
			}
		}

		private void MainWindow_Closed(object sender, EventArgs e)
		{
			try
			{
				if (!anulowano)
				{
					ZapisDoPliku zapis = new ZapisDoPliku(this.sciezka_pliku_do_zapisu);
					zapis.ZapiszMacierz(macierz_wynikowa);
				}
			}
			catch (FileFormatException){ }
			catch (IndexOutOfRangeException error)
			{
				MessageBox.Show(error.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			catch (FileNotFoundException) { }
		}
	}
}